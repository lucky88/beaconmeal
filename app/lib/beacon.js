var TiBeacons;
var androidPlatformTools;
var isForeground;
var appStateTimer;
var beaconsUuid = 'E2C56DB5-DFFB-48D2-B060-D0F5A71096E0';
var beaconsIdentifier = 'BeconMeal';
var startAdroidModule;
var callbacks = [];

//The two modules are initizialized.
if(OS_IOS)
{
	TiBeacons = require('org.beuckman.tibeacons');
} else 
if(OS_ANDROID)
{
	Ti.API.info('Init andoid module');
	TiBeacons = require("miga.tibeacon");
	TiBeacons.initBeacon({
    	success : onSuccess, 
    	error:onError, 
    	interval: 30, 
    	region: onRegion, 
    	found:onFound
	});
	androidPlatformTools = require('bencoding.android.tools').createPlatform(); 		
	setInterval( chechAppStatus, 1000);
}

//This function is called every one second to check if the Android is in foreground status or not.
function chechAppStatus()
{
	var isForeground = androidPlatformTools.isInForeground();
	Ti.App.Properties.setBool('isForeground', isForeground);
}

/**
 * The Properties module is used to store property/value pairs that persist beyond 
 * application sessions and device power cycles.
 */
function initProperties()
{
	//The property must be initialized only at first app launching
	if (!Ti.App.Properties.hasProperty('switchRaggingValue') && !Ti.App.Properties.hasProperty('switchMonitoringValue') && !Ti.App.Properties.hasProperty('isForeground')) 
	{
		Ti.API.info('First initializazion');
		Ti.App.Properties.setBool('isBluetoothOn', false);
		Ti.App.Properties.setBool('switchRaggingValue', false);
		Ti.App.Properties.setBool('switchMonitoringValue', false);
	} else 
	{
		if (Ti.App.Properties.getBool('switchMonitoringValue'))
		{
			monitoring(true);	
		}			
	}	
	Ti.API.info('Init method. The app is in foregroung: '+ Ti.App.Properties.getBool('isForeground'));
	Ti.App.Properties.setBool('isForeground', true);
}

initProperties();

/**
 * Function that turns on/off the beacon ranging. The right operation is chose on base of command value, if it is true the
 * ranging is turned on while if it is false it is turned off.
 * @param {Boolean} command boolen to indicate if the ranging must turn on or off.
 */
function ragging(command) 
{
	Ti.API.info('ragging!');
	if(OS_IOS)
	{
		if(command)
		{
			TiBeacons.startRangingForBeacons(
		   	{
		    	uuid : beaconsUuid,
		        identifier : beaconsIdentifier
		    });
			TiBeacons.addEventListener("beaconProximity", handleProximity);
		} else
		{		
			Ti.API.info('Stop ragging!');
			TiBeacons.stopRangingForBeacons(
			{
				uuid : beaconsUuid,
		        identifier : beaconsIdentifier
		    });
		    TiBeacons.removeEventListener("beaconProximity",  handleProximity);
		}  	
	}
}

/**
 * This function is called when monitor toggle button is tapped
 * @param {Object} command boolean that indicate the state of toggle 
 */
function monitoring(command)
{
	if(OS_IOS)
	{	
		Ti.App.Properties.setBool('switchMonitoringValue', command);
		if(command)
		{
			//If commnad is true than monitoring for region beaconsUuid must be started
			Ti.API.info('Start monitoring on iOS device!');
			TiBeacons.startMonitoringForRegion(
			{
	            uuid : beaconsUuid,
	            identifier : beaconsIdentifier
	        });	
	        TiBeacons.addEventListener("enteredRegion", enterRegion);
			TiBeacons.addEventListener("exitedRegion", exitRegion);
		} else
		{
			Ti.API.info('Stop monitoring on iOS device!');
			TiBeacons.stopMonitoringAllRegions();
	        TiBeacons.removeEventListener("enteredRegion", enterRegion);
			TiBeacons.removeEventListener("exitedRegion", exitRegion);
		}
	} else if(OS_ANDROID)
	{
		if(command)
		{
			Ti.API.info('Start monitoring on Android device!');
    		TiBeacons.startScanning();
    		
		} else
		{
			Ti.API.info('Stop monitoring on Android device!');
			TiBeacons.stopScanning();
		}
	}
};

/**
 * This function add to the array callback an object with a pair of values: one is the name of the event and the other is 
 * the name of function to call when the event occurs.
 */
function registerForEvent(eventName, _callback)
{
    if(_callback)
    {
    	var callback = {};
    	callback[eventName] = _callback;
        callbacks.push(callback);
	}
};

/**
 * This function remove from the array callbacks the object whose event name is equals to the parameter passed to the function.
 */
function unregisterForEvent(eventName)
{
    for (var key in callbacks) 
    { 	
    	var callback = callbacks[key];
  		if (callback.hasOwnProperty(eventName))
  		{
  			var index = callbacks.indexOf(callback);
  			callbacks.splice(index, 1);
  		}
	}
};

/**
 * Function callback for ragging event beaconProximity. This function is called only by the iOS module.
 * @param {Object} event object that contain information about beacon
 */
function handleProximity(event) 
{
	if(OS_IOS)
	{
		for (var key in callbacks) 
    	{ 	
    		var callback = callbacks[key];
  			if (callback.hasOwnProperty('updateProximity'))
  			{
  				callback.updateProximity(event);
  			}
		}
	}
}

/**
 * Function callback for monitoring event enteredRegion. This function is called only by the iOS module.
 * @param {Object} event object that contain information about beacon
 */
function enterRegion(e) 
{
	Ti.API.info('Enter in a region');
	if(OS_IOS)
	{
		/*
		 * If the app is in background and recive the enterRegion event it must send a local notification
		 * to notify to user the event
		 */
		if(!Ti.App.Properties.getBool('isForeground'))
		{
			Ti.API.info('Enter region in background mode!');
			var notification = Ti.App.iOS.scheduleLocalNotification(
			{
	        	alertBody:'A restaurant was founded tap to get detail!',
	        	alertAction:"View",
	        	date:new Date(new Date().getTime() + 500),
	        	sound: '/bubble.wav' 
			});
		} else
		{
			Ti.API.info('Enter region in foreground mode!');
			Ti.API.info('Start ranging!');
			ragging(true);
		}		
	}
}

/**
 * Function callback for monitoring event exitedRegion. This function is called only by the iOS module.
 * @param {Object} event object that contain information about beacon
 */
function exitRegion(e) 
{
	Ti.API.info('Exit from a region');
	if(OS_IOS)
	{
		//If the app is in isForeground and recive the enterRegion event it must send a local notification
		//to notify to user the event
		if(Ti.App.Properties.getBool('isForeground'))
		{
			Ti.API.info('Stop ranging!');
			ragging(false);
		}		
	}
}

//Callbacks m1ga android module
function onSuccess(e){}  

function onRegion(e){}  

function onFound(e)
{
  var proximity = e.device.proximity;
  proximity = proximity.toFixed(0);
  proximity = 'Approximately ' + proximity + ' m.';
  var beacon = {uuid: e.device.uuidDashed, major: e.device.major, minor: e.device.minor, proximity: proximity, rssi:e.device.rssi};
  if(e.device.uuidDashed ===  beaconsUuid)
  {
  	if(OS_ANDROID)
	{
		if(androidPlatformTools.isInForeground())
		{
			for (var key in callbacks) 
    		{ 	
    			var callback = callbacks[key];
  				if (callback.hasOwnProperty('onFound'))
  				{
  					callback.onFound(beacon);
  				}
			}	
		} else
		{
			var intent = Ti.Android.createIntent(
			{
    			flags : Ti.Android.FLAG_ACTIVITY_BROUGHT_TO_FRONT,
    			className : 'it.beaconProject.BeaconsmealActivity',
			});
			
			var pending = Titanium.Android.createPendingIntent({
    			intent: intent,
    			flags: Titanium.Android.FLAG_UPDATE_CURRENT
			});

			var notification = Titanium.Android.createNotification(
			{
				icon: Ti.App.Android.R.drawable.appicon,
				contentTitle: 'Restaurant found!',
				tickerText: 'Restaurant found!',
				contentText : 'A restaurant was founded tap to get detail!',
				when: new Date().getTime(),
				defaults:Titanium.Android.NotificationManager.DEFAULT_SOUND,
				contentIntent: pending
			});
			Titanium.Android.NotificationManager.notify(1, notification);
		}
	}
  }
}  

function onError(e)
{
	Ti.API.info(JSON.stringify(e));
}  


//IOS app life cycle. Used to determine if the application is in foreground status or not.
if(OS_IOS)
{
	Ti.App.addEventListener("pause", appPause);
	Ti.App.addEventListener("paused", appPaused);
	Ti.App.addEventListener("resume", appResume);
	Ti.App.addEventListener("resumed", appResumed);
}


function appPause() 
{
	Ti.API.info('App pause');
}

function appPaused() 
{
	Ti.API.info('App paused');
	if(OS_IOS)
	{
		Ti.App.Properties.setBool('isForeground', false);
	}
}

function appResume(e) 
{
	Ti.API.info('App resume');
}

function appResumed(e) 
{
	Ti.API.info('App resumed');
	if(OS_IOS)
	{
		Ti.App.Properties.setBool( 'isForeground', true);
		Ti.App.iOS.cancelAllLocalNotifications();		
	}
}

//IOS local notification
if(OS_IOS)
{
	Ti.App.iOS.addEventListener('notification', function(e)
	{
		Ti.API.info("local notification received: ");
		if(!Ti.App.Properties.getBool('switchRaggingValue'))
		{
			ragging(true);
			Ti.App.Properties.setBool( 'switchRaggingValue', true);
		}
	});	
}

//------------------ EXPORTED METHOD ----------------------
exports.ragging = ragging;
exports.monitoring = monitoring;
exports.registerForEvent = registerForEvent;
exports.unregisterForEvent = unregisterForEvent;