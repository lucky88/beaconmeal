//Initializazon of beacon
var beacon = require('beacon');

if(OS_IOS)
	beacon.registerForEvent("updateProximity", getRestourantData);
else if(OS_ANDROID)
	beacon.registerForEvent("onFound", getRestourantData);
	
var networkIsOnline, networkType;
var alertTitle = 'Beacons Meal';
var alertMessage = 'Beacons Meal needs access to the Internet and that the bluetooth is turned on!';

var restaurantList = Alloy.Collections.restaurant;
restaurantList.fetch();
$.win1.addEventListener('open', function()
{
	var isShowed = Ti.App.Properties.getBool('initMessageShowed');
	if(!isShowed)
	{
		var a = Titanium.UI.createAlertDialog({
    		title:alertTitle,
    		message:alertMessage
		});
		a.show();
		//This message is showed only at first start	
		Ti.App.Properties.setBool('initMessageShowed', true);
	}
	});
$.win1.open();


/**
 * Function that retrive from the RESTful the information about the restourant which corresponds to the current founded beacon.
 */
function getRestourantData(beaconData)
{
	//If proximity is 'unknown' doesn't download the data
	if(beaconData.proximity === 'unknown')
	{
		updateProximity(beaconData, undefined);
		return;
	}
	//Otherwise creates the id that is the hash of of the string uuid+major+minus and creates the url to download restaurant data
	var beaconId = Ti.Utils.md5HexDigest(beaconData.uuid + beaconData.major + beaconData.minor);
	var address = 'http://cs7036.comp.hkbu.edu.hk:5000/beacons/' + beaconId;
	var jsonData;
	//Then if there is a connection download the data
	var alreadyFouded = isAlreadyFounded(beaconId);
	if (Titanium.Network.online === true && Ti.App.Properties.getBool('isForeground') && !alreadyFouded.founded)
	{
		var request = Titanium.Network.createHTTPClient();
		
		request.onload = function requestReceived()
        {
        	var statusCode = request.status;
        	if (statusCode == 200)
        	{		
        		var restaurantData = JSON.parse(request.responseText);  
        		updateProximity(beaconData, restaurantData.ris);      		
        	} else
        	{
        		Ti.API.info('Errore: '+statusCode);
        	}
        };
        request.open('GET', address);
		request.send();
	} else if(!Titanium.Network.online)
	{
		alert('Internet connection is not present! Beacons Meal does\'n work without Internet connection!');
	} else if(alreadyFouded.founded)
	{
		alreadyFouded.model.set("beaconProximity", beaconData.proximity);	
	//This last is statement is for the android application.
	} else if(Ti.App.Properties.getBool('isForeground'))
	{
		Ti.API.info('App in background!');
	}
}

/**
 * Function that take an id like parameter and search on the collection if there is a corresponding object. If it exist true value
 * is returned false otherwise.
 * @param {String} e string that represents the id of the object inside the collection.
 */
function isAlreadyFounded(beaconId)
{
	var models = Alloy.Collections.restaurant.where({ id : beaconId});
	if (models.length == 0) 
		return {founded: false, model:null};
	else 
		return {founded:true, model:models[0]};
}
/*--------------------------------BEACON CODE---------------------------------*/

/**
 * Update the collection based on the data collected from the near beacons. If the proximity value is changed the it is update and
 * is refreshed the corresponding table row. If the beacon is the first time that is detected it is added to the collection and 
 * displayed inside the table. If the new value for the proximity parameter it is removed from collection e from table.
 */
function updateProximity(beaconData, restaurantData) 
{
	if(restaurantData !== undefined)
	{
		var model = ensureModel(beaconData, restaurantData);
		model.set("beaconProximity", beaconData.proximity);	
	} else if(beaconData.proximity === 'unknown' && restaurantData === undefined)
	{
		var id = Ti.Utils.md5HexDigest(beaconData.uuid + beaconData.major + beaconData.minor);		
		var models = Alloy.Collections.restaurant.where({ id : id });
		if (models.length == 1) 
		{			
			model = models[0];
			Alloy.Collections.restaurant.remove(model);
		}		
	} else
	{
		Ti.API.info('Error! ' + restaurantData);
	}
}

/**
 * Function that return the filled object of the collection. If it already exists it is directly retrived from the collection, otherwise
 * it is created.
 */
function ensureModel(beaconData, restaurantData)
{
	var atts = 
	{
		id : Ti.Utils.md5HexDigest(beaconData.uuid + beaconData.major + beaconData.minor),
		name : restaurantData.name,
		photoRist : restaurantData.photorist,
		beaconUuid : beaconData.uuid,
		beaconMajor : parseInt(beaconData.major),
		beaconMinor : parseInt(beaconData.minor),
		beaconProximity : beaconData.proximity,
		beaconRssi: parseInt(beaconData.rssi),
		menDescription: restaurantData.descr,
		menuPrice: restaurantData.price,
		photoMenu: restaurantData.photolink
	};
	
	var model;
	var models = Alloy.Collections.restaurant.where({ id : atts.id });
	
	if (models.length == 0) 
	{
		model = Alloy.createModel('restaurant', atts);
		Alloy.Collections.restaurant.add(model);
	} else 
	{
		model = models[0];
	}
	return model;
}

/**
* ------- This section contains the app navigation code --------
*/
function openOptions(e) 
{
    var optionsView = Alloy.createController('options').getView();
    if(OS_IOS)
    {
    	$.win1.openWindow(optionsView);
    } else if(OS_ANDROID)
    {
		optionsView.open(
		{
    		activityEnterAnimation: Ti.Android.R.anim.slide_in_left
		});
    } else 
    {
    	alert('This OS isn\'t supported');
    }  
}

/**
 * This method is called by TableView click event
 * @param {Object} e object contains the data of clicked row.
 */
function rowTapped(e)
{
	var models = Alloy.Collections.restaurant.where({ id : e.row.model });
	if (models.length == 1) 
	{			
		var model = JSON.parse(JSON.stringify(models[0]));
		var detailView = Alloy.createController('menuDetail', {
																model: model
															  }).getView();
		if(OS_IOS)
    	{
    		$.win1.openWindow(detailView);
    	} else if(OS_ANDROID)
    	{
    		detailView.open(
		{
    		activityEnterAnimation: Ti.Android.R.anim.slide_in_left
		});
    	}
	}
}