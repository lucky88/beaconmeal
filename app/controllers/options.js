var args = arguments[0] || {};
var beacon = require('beacon');

if(OS_IOS)
	$.raggingSwitch.value = Ti.App.Properties.getBool('switchRaggingValue');
$.monitoringSwitch.value = Ti.App.Properties.getBool('switchMonitoringValue');

var beaconsList = Alloy.Collections.instance('beacon');

if(OS_ANDROID)
{
	$.optionWin.addEventListener('open',function()
	{
		var actionBar = $.optionWin.activity.actionBar; // get a handle to the action bar
		actionBar.onHomeIconItemSelected = function() 
		{ // what to do when the "home" icon is pressed
			$.optionWin.close({activityExitAnimation: Ti.Android.R.anim.slide_out_right});
	    };	
	});
}

function ragging()
{
	var value = $.raggingSwitch.value;
	Ti.App.Properties.setBool('switchRaggingValue', value);
	var started = beacon.ragging($.raggingSwitch.value);
}

/**
 * Callback functtion for the event onChanged of monitor Switch
 */
function monitoring()
{
	var value = $.monitoringSwitch.value;
	Ti.App.Properties.setBool('switchMonitoringValue', value);
	beacon.monitoring($.monitoringSwitch.value);
}