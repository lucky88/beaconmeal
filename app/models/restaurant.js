exports.definition = {
	config: {
		columns: {
			"id": "TEXT PRIMARY KEY",
		    "name": "TEXT",
		    "photoLink": "TEXT",
		    "beaconUuid": "TEXT",
		    "beaconMajor": "INTEGER",
		    "beaconMinor": "INTEGER",
		    "beaconProximity": "TEXT",
		    "beaconRssi": "INTEGER",
		    "menDescription": "TEXT",
		    "menuPrice": "INTEGER",
		    "photoMenu": "TEXT"
		},

		adapter: {
			type: "sql",
			collection_name: "restaurant",
			idAttribute: "id"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) 
	{
		_.extend(Collection.prototype, {
			comparator: function(model) 
			{
        		return model.get('beaconRssi');
    		}
		});

		return Collection;
	}
};